README


Python3 amd internet connection required!

1 clone repo

2 create virtualenv

3 activate env

4 pip install -r requirements.txt

5 install and run postgresql

6 create database and psql user properly: https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04

    dbname, username and password must be "netguru",  host: localhost

7 run migrations

8 run server

How to use:


GET /api/movies/ endpoint returns all movies in database, filtering is also possible.
    This is list of all available filters (django like syntax):

                {'id': ['exact', 'lt', 'lte', 'gt', 'gte'],
                'year': ['exact', 'lt', 'lte', 'gt', 'gte'],
                'title': ['exact', 'icontains', 'startswith'],
                'imdbid': ['exact', 'icontains', 'startswith'],
                'rated': ['exact', 'icontains', 'startswith', 'isnull'],
                'released': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                             'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                             'day__gte', 'day__lt', 'day__lte', 'isnull'],
                'runtime': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
                'genre': ['exact', 'icontains', 'startswith', 'isnull'],
                'director': ['exact', 'icontains', 'startswith', 'isnull'],
                'writer': ['exact', 'icontains', 'startswith', 'isnull'],
                'actors': ['isnull'],
                'actors__name': ['exact', 'icontains', 'startswith'],
                'plot': ['exact', 'icontains', 'startswith', 'isnull'],
                'language': ['exact', 'icontains', 'startswith', 'isnull'],
                'country': ['exact', 'icontains', 'startswith', 'isnull'],
                'awards': ['exact', 'icontains', 'startswith', 'isnull'],
                'ratings': ['isnull'],
                'ratings__source': ['exact', 'icontains', 'startswith'],
                'ratings__value': ['exact', 'icontains', 'startswith'],
                'metascore': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
                'imdbrating': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
                'imdbvotes': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
                'type': ['exact', 'icontains', 'startswith', 'isnull'],
                'dvd': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                        'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                        'day__gte', 'day__lt', 'day__lte', 'isnull'],
                'boxoffice': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
                'production': ['exact', 'icontains', 'startswith', 'isnull'],
                'website': ['exact', 'icontains', 'startswith', 'isnull'],
                'comments__content': ['exact', 'icontains', 'startswith'],
                'comments__date': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                                   'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                                   'day__gte', 'day__lt', 'day__lte', 'hour__exact', 'hour__gt', 'hour__gte', 'hour__lte',
                                   'hour__lt', 'minute__exact', 'minute__lt', 'minute__lte', 'minute__gt', 'minute__gte',
                                   'second__exact', 'second__lt', 'second__lte', 'second__gt', 'second__gte'],
                'comments': ['isnull']}

    Keys in this dictionary represents fields of models you can relate to, they are also basic part of filter usage.
    Values in this dictionary represent different operations of filtering.

    For example if you want to get all films released afters year 2000, and without comments, you can request:
    GET /api/movies/?released__year__gt=2000&comments__isnull=True

    description of options given here is availave at:
    https://docs.djangoproject.com/pl/2.1/ref/models/querysets/#field-lookups

    The successful response contains:
        {
        "id": Database ID of film (string)
        "title":(string)
        "year":(int)
        "rated":(string)
        "released":(date)
        "runtime": (int) in minutes
        "genre": (string)
        "director": (string)
        "writer":(string)
        "actors": [
          {
            "name":(string)
          },
          {
            "name": "(string)
          },
          {
            "name": "(string)
          },
          {
            "name": "(string)
          }
        ],
        "plot":(string)
        "language":(string)
        "country":(string)
        "awards":(string)
        "poster": url path (string)
        "ratings": [
          {
            "source":(string)
            "value":(string)
          },
          {
            "source":
            "value":
          },
          {
            "source":
            "value":
          }
        ],
        "metascore":(int)
        "imdbrating":(float)
        "imdbvotes":(int)
        "imdbid": ID in im database (string)
        "type":(string)
        "dvd":(date)
        "boxoffice": (int) in dollars
        "production": (string)
        "website": url path (string)
        "comments": [
          {
            "id": Comment id in database (int)
            "date":  (date and time)
            "content": (string)
          },
          {
            "id":
            "date":
            "content":
          }
        ]
      },




POST api/movies/ this endpoint requires "movie_title" parameter in request body.

    This parameter is used to fetch data from external API and save them to netguru application database.
    With every request the attempt to fetch more movies containing given phrase is made.
    Always first fetched movie object is returned.

    Possible response status codes:
    400 - "movie_title" parameter not supplied
    404 - movie with requested title not found
    201 - movies fetched and saved to database, response contains full Movie object (like in /GET response)
    204 - requested movies already in database, no CREATE performed on database





GET api/comments/ endpoint returns all comments in database, filtering by associated movie is also possible.

    To filter by movie pass "movie" parameter, which contains movie ID in database
    List of available filters:
    'movie': ['exact', 'lt', 'lte', 'gt', 'gte']

    Example:
        To get comments on movie with id=7, request GET api/comments/?movie=7
        response:
        [{
            "id": 1, (Comment ID in database)
            "date": "2018-08-15T21:36:53.521163Z",
            "content": "Good one",
            "movie": 7, (associated movie ID in database)
            "movie_title": "Requiem for a Dream"
          }]




POST api/comments/ this endpoint requires following parameters in request body:
    "content"(the commented text) and "movie"(movie ID one want to comment on)

    Endpoint performs creation of comment, and adding it to chosen movie.
    Successful response contains object like in GET api/comments/




TESTING:
    There are number of automatic test written, to run them type: ./manage.py test
    For more details check functions docs in file /src/movies/test.py