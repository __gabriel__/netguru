from django.urls import path
from .views import MovieView, CommentsView

app_name = "movies"

urlpatterns = [
    path('movies/', MovieView.as_view(), name="movies"),
    path('comments/', CommentsView.as_view(), name="comments"),
]
