from django.core.exceptions import ValidationError
import datetime


def validate_max_year(value):
    if value > datetime.datetime.now().year:
        raise ValidationError("Future date is forbidden!")
