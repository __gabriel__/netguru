from django.test import TestCase
from django.urls import reverse
from .models import Movie, Comment


class MovieViewTests(TestCase):

    def test_no_required_parameter(self):
        """
        Test if response code is 400 when, no required parameter has been given.
        """
        url = reverse('api:movies')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 400)

    def test_strange_title(self):
        """
        Test if response code is 404 when move has not been found.
        """
        url = reverse('api:movies')
        response = self.client.post(url, {'movie_title': '1245!@#%asdgv'})
        self.assertEqual(response.status_code, 404)

    def test_downloading(self):
        """
        Test if api saves movies to database.
        """
        url = reverse('api:movies')
        response = self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        self.assertTrue(Movie.objects.all().exists())
        self.assertEqual(response.status_code, 201)

    def test_downloading_twice_same_movies(self):
        """
        Test if api returns 204 when movies existing in database are POSTed once more.
        """
        url = reverse('api:movies')
        response = self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        self.assertTrue(Movie.objects.all().exists())
        self.assertEqual(response.status_code, 201)
        response = self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        self.assertEqual(response.status_code, 204)

    def test_basic_retrieving_movies(self):
        """
        Test if api returns all movies from database on GET call.
        """
        url = reverse('api:movies')
        self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        count = Movie.objects.all().count()
        self.assertNotEqual(count, 0)
        response = self.client.get(url)
        self.assertEqual(len(response.json()), count)

    def test_retrieving_movies_with_filters(self):
        """
        Test if api returns filtered movie from database on GET call.
        """
        url = reverse('api:movies')
        self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        response = self.client.get(url, {"imdbid": "tt0180093"})
        response = response.json()
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["imdbid"], "tt0180093")


class CommentsViewTests(TestCase):

    def test_no_required_parameters(self):
        """
        Test if response code is 400 when, no required parameters has been given.
        """
        url = reverse('api:comments')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 400)

    def test_add_comment_to_nonexisting_movie(self):
        """
        Test if response code is 400 when required parameters are present, but Movie does not exists.
        """
        url = reverse('api:comments')
        response = self.client.post(url, {"movie": 100, "content": "xxxxxxxxxx"})
        self.assertEqual(response.status_code, 400)

    def test_add_successful_adding_comment(self):
        """
        Test if comment can be added.
        """
        url = reverse('api:movies')
        self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        url = reverse('api:comments')
        self.client.post(url, {"movie": 1, "content": "xxxxxxxxxx"})
        self.assertEqual(Comment.objects.first().content, "xxxxxxxxxx")

    def test_list_comments(self):
        """
        Test if api returns all comments from database on GET call.
        """
        url = reverse('api:movies')
        self.client.post(url, {'movie_title': 'Requiem for a Dream'})
        movie_id = Movie.objects.first().id
        url = reverse('api:comments')
        self.client.post(url, {"movie": movie_id, "content": "xxxxxxxxxx"})
        self.client.post(url, {"movie": movie_id, "content": "yyyyyyyyyy"})
        response = self.client.get(url)
        self.assertEqual(len(response.json()), 2)

