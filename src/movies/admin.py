from django.contrib import admin
from .models import Movie, Comment, Actor, Rating


admin.site.register(Rating)
admin.site.register(Actor)
admin.site.register(Comment)
admin.site.register(Movie)
