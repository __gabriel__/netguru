from .models import Movie, Comment
import django_filters


class MovieFilter(django_filters.FilterSet):

    class Meta:
        model = Movie
        fields = {
            'id': ['exact', 'lt', 'lte', 'gt', 'gte'],
            'year': ['exact', 'lt', 'lte', 'gt', 'gte'],
            'title': ['exact', 'icontains', 'startswith'],
            'imdbid': ['exact', 'icontains', 'startswith'],
            'rated': ['exact', 'icontains', 'startswith', 'isnull'],
            'released': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                         'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                         'day__gte', 'day__lt', 'day__lte', 'isnull'],
            'runtime': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
            'genre': ['exact', 'icontains', 'startswith', 'isnull'],
            'director': ['exact', 'icontains', 'startswith', 'isnull'],
            'writer': ['exact', 'icontains', 'startswith', 'isnull'],
            'actors': ['isnull'],
            'actors__name': ['exact', 'icontains', 'startswith'],
            'plot': ['exact', 'icontains', 'startswith', 'isnull'],
            'language': ['exact', 'icontains', 'startswith', 'isnull'],
            'country': ['exact', 'icontains', 'startswith', 'isnull'],
            'awards': ['exact', 'icontains', 'startswith', 'isnull'],
            'ratings': ['isnull'],
            'ratings__source': ['exact', 'icontains', 'startswith'],
            'ratings__value': ['exact', 'icontains', 'startswith'],
            'metascore': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
            'imdbrating': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
            'imdbvotes': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
            'type': ['exact', 'icontains', 'startswith', 'isnull'],
            'dvd': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                    'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                    'day__gte', 'day__lt', 'day__lte', 'isnull'],
            'boxoffice': ['exact', 'lt', 'lte', 'gt', 'gte', 'isnull'],
            'production': ['exact', 'icontains', 'startswith', 'isnull'],
            'website': ['exact', 'icontains', 'startswith', 'isnull'],
            'comments__content': ['exact', 'icontains', 'startswith'],
            'comments__date': ['year__exact', 'year__lt', 'year__lte', 'year__gt', 'year__gte', 'month__exact',
                               'month__gt', 'month__gte', 'month__lt', 'month__lte', 'day__exact', 'day__gt',
                               'day__gte', 'day__lt', 'day__lte', 'hour__exact', 'hour__gt', 'hour__gte', 'hour__lte',
                               'hour__lt', 'minute__exact', 'minute__lt', 'minute__lte', 'minute__gt', 'minute__gte',
                               'second__exact', 'second__lt', 'second__lte', 'second__gt', 'second__gte'],
            'comments': ['isnull']
        }


class CommentsFilter(django_filters.FilterSet):

    class Meta:
        model = Comment
        fields = {
            'movie': ['exact', 'lt', 'lte', 'gt', 'gte']
        }
