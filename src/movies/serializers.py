from rest_framework import serializers
from .models import Movie, Rating, Actor, Comment


class RatingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rating
        fields = ('source', 'value')


class ActorsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Actor
        fields = ('name',)


class CommentsSerializerWithMovie(serializers.ModelSerializer):
    movie_title = serializers.SerializerMethodField(read_only=True)

    def get_movie_title(self, obj):
        return obj.movie.title

    class Meta:
        model = Comment
        fields = ('id', 'date', 'content', 'movie', 'movie_title')


class CommentsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('id', 'date', 'content',)


class MovieSerializer(serializers.ModelSerializer):
    ratings = RatingsSerializer(many=True, required=False, read_only=True)
    actors = ActorsSerializer(many=True, required=False, read_only=True)
    comments = CommentsSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Movie
        fields = ('id', 'title', 'year', 'rated', 'released', 'runtime', 'genre', 'director', 'writer', 'actors', 'plot',
                  'language', 'country', 'awards', 'poster', 'ratings', 'metascore', 'imdbrating', 'imdbvotes',
                  'imdbid', 'type', 'dvd', 'boxoffice', 'production', 'website', 'comments')




