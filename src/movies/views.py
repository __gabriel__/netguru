from rest_framework.generics import ListCreateAPIView, ListCreateAPIView
from rest_framework.response import Response
from rest_framework import status
from django_filters import rest_framework as filters
from netguru.settings import omdb_api_key
from .models import Movie, Actor, Rating, Comment
from .serializers import MovieSerializer, CommentsSerializerWithMovie
from .filters import MovieFilter, CommentsFilter
import requests
import grequests
import json
import datetime


class MovieView(ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MovieFilter

    def create(self, request, *args, **kwargs):
        title = request.data.get('movie_title')
        if not title:
            return Response("movie_title parameter is required", status=status.HTTP_400_BAD_REQUEST)

        base_url = 'http://www.omdbapi.com/?apikey={}&r=json'.format(omdb_api_key)
        search_request_url = '{}&s={}'.format(base_url, title)
        fetched = requests.get(search_request_url)
        data = fetched.json()
        if data["Response"] == "False":
            return Response(data['Error'], status=status.HTTP_404_NOT_FOUND)

        url_list = ['{}&plot=full&i={}'.format(base_url, movie['imdbID']) for movie in data["Search"]]
        rs = (grequests.get(u) for u in url_list)
        results = grequests.map(rs, exception_handler=exception_handler)
        results = [result for result in results if getattr(result, 'status_code', -1) == 200]
        first = True
        for result in results:
            data = json.loads(result.content.decode('utf-8'))
            request_result = data.pop("Response")
            if request_result == "True":
                if Movie.objects.filter(imdbid=data['imdbID']).exists():
                    continue
                actors = data.pop("Actors").split(",") if data["Actors"] != "N/A" else None
                ratings = data.pop("Ratings") if data["Ratings"] else None
                data_to_serialize = {k.lower(): None if v == 'N/A' else v for k, v in data.items()}
                data_to_serialize.update({'year': int(data_to_serialize['year'][:4]),
                                          'runtime': int(data_to_serialize['runtime'].replace(" min", "")) if
                                          data_to_serialize.get('runtime') else None,
                                          'imdbvotes': int(data_to_serialize['imdbvotes'].replace(",", "")) if
                                          data_to_serialize.get('imdbvotes') else None,
                                          'metascore': int(data_to_serialize['metascore'].replace(",", "")) if
                                          data_to_serialize.get('metascore') else None,
                                          'boxoffice': int(data_to_serialize['boxoffice'][1:].replace(",", "")) if
                                          data_to_serialize.get('boxoffice') else None,
                                          'imdbrating': float(data_to_serialize['imdbrating']) if
                                          data_to_serialize.get('imdbrating') else None,
                                          'released': datetime.datetime.strptime(data_to_serialize['released'], '%d %b %Y').date() if
                                          data_to_serialize.get('released') else None,
                                          'dvd': datetime.datetime.strptime(data_to_serialize['dvd'], '%d %b %Y').date() if
                                          data_to_serialize.get('dvd') else None}
                                         )
                serializer = MovieSerializer(data=data_to_serialize)
                if serializer.is_valid():
                    obj = serializer.save()
                    if actors:
                        actors = [Actor.objects.get_or_create(name=actor_name)[0] for actor_name in actors]
                        obj.actors.add(*actors)
                    if ratings:
                        ratings = [Rating.objects.get_or_create(
                            source=rating["Source"], value=rating["Value"])[0] for rating in ratings]
                        obj.ratings.add(*ratings)
                    if first:
                        first_obj = obj
                        first = False
        try:
            return Response(MovieSerializer(first_obj).data, status=status.HTTP_201_CREATED)
        except NameError:
            return Response("No objects has been created", status=status.HTTP_204_NO_CONTENT)


def exception_handler(request, exception):
    print("Request failed!")


class CommentsView(ListCreateAPIView):
    serializer_class = CommentsSerializerWithMovie
    queryset = Comment.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = CommentsFilter
