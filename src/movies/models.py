from django.db import models
from django.core.validators import MinValueValidator
from .validators import validate_max_year


class Movie(models.Model):
    imdbid = models.CharField(max_length=20, unique=True)
    title = models.CharField(max_length=100,)
    year = models.IntegerField(validators=[MinValueValidator(1878), validate_max_year],)
    rated = models.CharField(max_length=10, blank=True, null=True)
    released = models.DateField(blank=True, null=True)
    runtime = models.IntegerField(blank=True, null=True)
    genre = models.CharField(max_length=50, blank=True, null=True)
    director = models.CharField(max_length=40, blank=True, null=True)
    writer = models.CharField(max_length=300, blank=True, null=True)
    actors = models.ManyToManyField('Actor', blank=True)
    plot = models.TextField(blank=True, null=True)
    language = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    awards = models.TextField(blank=True, null=True)
    poster = models.TextField(blank=True, null=True)
    ratings = models.ManyToManyField('Rating', blank=True)
    metascore = models.IntegerField(validators=[MinValueValidator(0)], blank=True, null=True)
    imdbrating = models.FloatField(validators=[MinValueValidator(0)], blank=True, null=True)
    imdbvotes = models.IntegerField(validators=[MinValueValidator(0)], blank=True, null=True)
    type = models.CharField(max_length=20, blank=True, null=True)
    dvd = models.DateField(blank=True, null=True)
    boxoffice = models.BigIntegerField(blank=True, null=True)
    production = models.CharField(max_length=100, blank=True, null=True)
    website = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        return self.title


class Rating(models.Model):
    source = models.CharField(max_length=60,)
    value = models.CharField(max_length=20)

    def __str__(self):
        return "Rated {} : by {}".format(self.value, self.source)


class Actor(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name


class Comment(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return self.content[:25]

